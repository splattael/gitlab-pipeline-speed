require "terminal-table"

class Downloader
  require "gitlab"
  require "json"
  require "http"

  PROJECT = "gitlab-org/gitlab"

  ARTIFACTS_BASE_URL = "https://gitlab-org.gitlab.io/-/gitlab/-/jobs/%{job_id}/artifacts/knapsack/report-master.json"

  def initialize(pat)
    @pat = pat
    @report = nil
  end

  def job_id
    @job_id ||= fetch_job_id
  end

  def job_id=(job_id)
    @job_id = job_id
    @report = nil
  end

  def report
    @report ||= fetch_report
  end

  private

  def get_rspec_job(pipeline)
    id = pipeline.id

    Gitlab.pipeline_jobs(PROJECT, id).auto_paginate do |job|
      return job if job.name =~ /^rspec(?:-ee)? unit/
    end

    nil
  end

  def get_json(uri, args = {})
    url = uri % args.merge(project_id: PROJECT)

    HTTP
      .get(url, headers: { 'Private-Token' => @pat })
      .to_s
      .then(&JSON.method(:parse))
  rescue JSON::ParserError
  end

  def fetch_report
    get_json(ARTIFACTS_BASE_URL, job_id: job_id)
  end

  def fetch_job_id
    Gitlab.pipelines(PROJECT, status: "success", ref: "master").auto_paginate do |pipeline|
      job = get_rspec_job(pipeline)
      return job.id if job
    end
  end
end

def as_table(rows)
  puts Terminal::Table.new(rows: rows)
end

def top(list, n: 30)
  as_table list.sort_by(&:last).last(n).reverse
end

def search(list, *names, searcher: :select)
  names = names.map { |name| Regexp === name ? name : %r{^(ee/)?#{name}} }
  regexp = Regexp.union(names)
  list.public_send(searcher) { |n, _| regexp.match?(n) }
end

def exclude(list, *names)
  search(list, *names, searcher: :reject)
end

def sum_grouped(list)
  list
    .group_by { |name, _| File.dirname(name) }
    .map { |name, values| [name, values.map(&:last).sum] }
end

def job_id
  downloader.job_id
end

def set_job_id(job_id)
  downloader.job_id = job_id
end

def specs
  downloader.report
end

def downloader
  @dowloader ||= Downloader.new(ENV.fetch('GITLAB_API_PRIVATE_TOKEN'))
end

if $0 == __FILE__
  require "irb"
  IRB.start
end
