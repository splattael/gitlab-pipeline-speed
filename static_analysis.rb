
require "bundler/setup"
require "gitlab"

PROJECT = "gitlab-org/gitlab-ee"
RUBOCOP_LINE = %r{bundle exec rubocop --parallel\n.*?Finished in (\d+\.\d+) seconds}
PAGE = 360

def extract_rubocop_duration(job)
  trace = Gitlab.job_trace(PROJECT, job.id)
  seconds = RUBOCOP_LINE.match(trace)[1]
  return "?" unless seconds

  Float(seconds)
end

Gitlab.jobs(PROJECT, scope: "success", page: PAGE, per_page: 100).auto_paginate do |job|
  job.name == "static-analysis" or next

  rubocop_duration = extract_rubocop_duration(job)

  puts [job.created_at, job.duration, rubocop_duration, job.web_url].join("\t")
end
