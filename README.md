# GitLab Pipeline speed

## Setup

## Spec runtime

```
$ ruby spec_runtime.rb
```

### Examples

```
>> top specs
+--------------------------------------------------------------------------------------+--------------------+
| spec/services/notification_service_spec.rb                                           | 795.223219871521   |
| spec/requests/api/merge_requests_spec.rb                                             | 384.25068974494934 |
| spec/features/projects/jobs_spec.rb                                                  | 307.13077092170715 |
| spec/features/merge_request/user_resolves_diff_notes_and_discussions_resolve_spec.rb | 246.51749467849731 |
| spec/features/expand_collapse_diffs_spec.rb                                          | 225.54562973976135 |
| spec/features/boards/boards_spec.rb                                                  | 223.32506799697876 |
| spec/features/projects/pipelines/pipelines_spec.rb                                   | 214.6836769580841  |
| spec/features/projects/pipelines/pipeline_spec.rb                                    | 210.0240876674652  |
| ee/spec/features/projects/services/prometheus_custom_metrics_spec.rb                 | 202.53355717658997 |
| ee/spec/features/issuables/related_issues_spec.rb                                    | 200.24994468688965 |
| spec/services/merge_requests/push_options_handler_service_spec.rb                    | 200.05303573608398 |
| spec/features/discussion_comments/issue_spec.rb                                      | 192.4712643623352  |
| ee/spec/features/protected_branches_spec.rb                                          | 190.88211059570312 |
| ee/spec/features/boards/scoped_issue_board_spec.rb                                   | 185.9924623966217  |
| spec/features/merge_request/user_sees_merge_request_pipelines_spec.rb                | 179.89520621299744 |
| spec/models/merge_request_spec.rb                                                    | 178.6507670879364  |
| spec/features/issues_spec.rb                                                         | 172.49592351913452 |
| spec/features/oauth_login_spec.rb                                                    | 166.27431678771973 |
| spec/requests/api/projects_spec.rb                                                   | 163.9661774635315  |
| spec/features/issues/user_uses_quick_actions_spec.rb                                 | 160.45886373519897 |
| spec/features/merge_request/user_posts_diff_notes_spec.rb                            | 160.1043667793274  |
| spec/lib/gitlab/cycle_analytics/events_spec.rb                                       | 151.52877926826477 |
| spec/requests/api/users_spec.rb                                                      | 149.94117641448975 |
| spec/mailers/notify_spec.rb                                                          | 146.8423776626587  |
| spec/features/issuables/markdown_references/jira_spec.rb                             | 145.5180504322052  |
| spec/features/issues/filtered_search/filter_issues_spec.rb                           | 143.1648199558258  |
| spec/features/u2f_spec.rb                                                            | 142.1792755126953  |
| ee/spec/lib/gitlab/elastic/search_results_spec.rb                                    | 138.7183666229248  |
| spec/requests/api/pipelines_spec.rb                                                  | 137.20367884635925 |
| ee/spec/models/approval_state_spec.rb                                                | 131.05846333503723 |
+--------------------------------------------------------------------------------------+--------------------+
```

```
>> top specs, n: 10
>> top sum_grouped(specs)

>> top sum_grouped(exclude(specs, "spec/features"))
+-----------------------------------+--------------------+
| spec/requests/api                 | 2192.1877143383026 |
| spec/models                       | 1345.91650390625   |
| spec/services                     | 1028.5150275230408 |
| spec/services/merge_requests      | 684.7760813236237  |
| spec/controllers/projects         | 641.475094795227   |
| ee/spec/models                    | 588.5042600631714  |
| ee/spec/requests/api              | 441.3658924102783  |
| spec/models/cycle_analytics       | 395.7590970993042  |
| spec/lib/gitlab                   | 367.7309784889221  |
| spec/models/concerns              | 335.53375124931335 |
| spec/finders                      | 334.93934392929077 |
| ee/spec/controllers/projects      | 323.2573034763336  |
| spec/services/ci                  | 304.3930335044861  |
| spec/lib/gitlab/import_export     | 296.59612345695496 |
| spec/services/projects            | 284.8351242542267  |
| spec/models/ci                    | 268.8635084629059  |
| spec/requests/api/issues          | 267.0978956222534  |
| spec/lib/gitlab/cycle_analytics   | 248.17885899543762 |
| spec/serializers                  | 213.9483289718628  |
| spec/controllers                  | 209.8893222808838  |
| spec/models/clusters/applications | 208.10819482803345 |
| spec/lib/banzai/filter            | 204.67069602012634 |
| spec/models/project_services      | 197.3375358581543  |
| spec/policies                     | 194.87507700920105 |
| spec/requests                     | 187.17496943473816 |
| spec/helpers                      | 184.78121042251587 |
| spec/workers                      | 180.12196397781372 |
| ee/spec/policies                  | 168.80632066726685 |
| ee/spec/lib/gitlab/elastic        | 165.33105087280273 |
| spec/services/issues              | 161.4434416294098  |
+-----------------------------------+--------------------+
```

```
>> top search(specs, "spec/requests/api"), n: 30
+-------------------------------------------------------+--------------------+
| spec/requests/api/merge_requests_spec.rb              | 384.25068974494934 |
| spec/requests/api/projects_spec.rb                    | 163.9661774635315  |
| spec/requests/api/users_spec.rb                       | 149.94117641448975 |
| spec/requests/api/pipelines_spec.rb                   | 137.20367884635925 |
| spec/requests/api/groups_spec.rb                      | 121.91621685028076 |
| spec/requests/api/runners_spec.rb                     | 94.1166410446167   |
| spec/requests/api/issues/get_project_issues_spec.rb   | 92.81740164756775  |
| spec/requests/api/issues/get_group_issues_spec.rb     | 89.68120741844177  |
| spec/requests/api/commits_spec.rb                     | 86.88920593261719  |
| spec/requests/api/badges_spec.rb                      | 64.11868977546692  |
| spec/requests/api/runner_spec.rb                      | 63.22032117843628  |
| spec/requests/api/todos_spec.rb                       | 58.12593173980713  |
| spec/requests/api/members_spec.rb                     | 57.227678298950195 |
| spec/requests/api/discussions_spec.rb                 | 52.16208791732788  |
| ee/spec/requests/api/search_spec.rb                   | 50.12327456474304  |
| spec/requests/api/branches_spec.rb                    | 46.113054513931274 |
| spec/requests/api/files_spec.rb                       | 43.21229147911072  |
| spec/requests/api/issues/issues_spec.rb               | 42.12006139755249  |
| spec/requests/api/releases_spec.rb                    | 41.66474008560181  |
| spec/requests/api/notes_spec.rb                       | 39.18894934654236  |
| spec/requests/api/project_clusters_spec.rb            | 35.28412675857544  |
| spec/requests/api/helpers_spec.rb                     | 33.912911891937256 |
| spec/requests/api/wikis_spec.rb                       | 32.82317137718201  |
| ee/spec/requests/api/v3/github_spec.rb                | 32.62819790840149  |
| spec/requests/api/project_export_spec.rb              | 30.87746000289917  |
| spec/requests/api/tags_spec.rb                        | 30.531959295272827 |
| spec/requests/api/jobs_spec.rb                        | 27.83815884590149  |
| spec/requests/api/issues/post_projects_issues_spec.rb | 27.830809593200684 |
| ee/spec/requests/api/maven_packages_spec.rb           | 25.915703058242798 |
| spec/requests/api/services_spec.rb                    | 24.715665578842163 |
+-------------------------------------------------------+--------------------+
```
