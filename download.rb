
require "uri"
gem "http", "4.1.1"
require "http"
require "json"

PAT = ENV.fetch('GITLAB_API_PRIVATE_TOKEN')
PROJECT = "gitlab-org%2fgitlab"

BASE_URL = "https://gitlab.com/api/v4/projects/%{project_id}/pipelines"
ARTIFACTS_BASE_URL = "https://gitlab-org.gitlab.io/-/gitlab/-/jobs/%{job_id}/artifacts/knapsack/gitlab/rspec_report-master%{ee}.json"

def get(uri, args = {})
  url = uri % args.merge(project_id: PROJECT)

  p url: url

  HTTP.get(url, headers: { 'Private-Token' => PAT })
    .to_s
    .then(&JSON.method(:parse))
end

def pipelines(page)
  url = "#{BASE_URL}?status=success&per_page=10&page=#{page}&ref=master"

  get(url)
end

def pipeline(pipeline)
  id, sha = pipeline.values_at("id", "sha")

  job = jobs(id).find { |job| job["name"] =~ /^rspec unit/ }
  unless job
    puts "  nope"
    return
  end
  job_id, started_at = job.values_at("id", "started_at")
  p pipeline_id: id, started_at: started_at, job_id: job_id

  save("#{id}-#{started_at}-#{sha}") do
    artifact(job_id, "").merge(artifact(job_id, "-ee"))
  end
rescue JSON::ParserError
  puts "  failed"
end

def jobs(pipeline_id)
  url = "#{BASE_URL}/%{pipeline_id}/jobs"

  get(url, pipeline_id: pipeline_id)
end

def artifact(job_id, ee)
  get(ARTIFACTS_BASE_URL, job_id: job_id, ee: ee)
end

def save(name)
  if File.exists?("./data/#{name}.json")
    puts "  cached"
    return
  end

  json = yield

  File.write("./data/#{name}.json", JSON.pretty_generate(json))
end

1.upto(1000) do |page|
  p page: page
  pp = pipelines(page).first
  pipeline(pp)
end
